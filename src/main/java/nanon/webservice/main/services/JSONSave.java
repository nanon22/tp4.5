package nanon.webservice.main.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import nanon.webservice.main.models.Gare;
import nanon.webservice.main.models.MetroInstance;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

@Service
public class JSONSave implements nanon.webservice.main.services.GareService {

    @Override
    public void save(MetroInstance gare) throws IOException {

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(new File("/home/nanon/IdeaProjects/data/gares.json"), gare);
        }
        catch (IOException e) {
            System.err.println(":(");
            e.printStackTrace();
        }

    }

    @Override
    public Gare load() throws IOException {
        Gare gare = null;

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            gare = objectMapper.readValue(new File("/home/nanon/IdeaProjects/data/gares.json"), Gare.class);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return gare;
    }
}
