package nanon.webservice.main.services;

import nanon.webservice.main.models.Gare;
import nanon.webservice.main.models.MetroInstance;

import java.io.IOException;
import java.util.prefs.Preferences;

public interface GareService {
    void save(MetroInstance metroInstance) throws IOException;

    MetroInstance load() throws IOException;
}
