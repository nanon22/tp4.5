package nanon.webservice.main.models;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Train implements MetroInstance {

    String destination;
    int numero;
    LocalDateTime depart;
    LocalDateTime arrivee;
    public static List<Train> trains;

    public Train() {
        if (trains == null) {
            trains = new ArrayList<Train>();
        }
        trains.add(this);
    }

    public Train(String dest, int num, LocalDateTime dep, LocalDateTime arr) {
        destination = dest;
        numero = num;
        depart = dep;
        arrivee = arr;

        if (trains == null) {
            trains = new ArrayList<Train>();
        }
        trains.add(this);
    }

    @Override
    public String toString() {
        String train = "Train " + numero + " à destination de : " + destination + ", départ : " + depart + ", Arrivée : " + arrivee + ".";

        return train;
    }

    public String getDestination() { return  destination; }

    public void setDestination(String Value) {
        destination = Value;
    }

    public int getNumero() { return  numero; }

    public void setNumero(int Valeur) {
        numero = Valeur;
    }

    public LocalDateTime getDepart() { return  depart; }

    public void setDepart(LocalDateTime Valeur) {
        depart = Valeur;
    }

    public LocalDateTime getArrivee() { return  depart; }

    public void setArrivee(LocalDateTime Valeur) {
        arrivee = Valeur;
    }
}
