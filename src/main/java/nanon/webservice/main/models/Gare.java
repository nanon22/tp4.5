package nanon.webservice.main.models;

import java.util.ArrayList;
import java.util.List;

public class Gare implements MetroInstance {
    String name;
    public static List<Gare> gares;

    public Gare() {
        if (gares == null) {
            gares = new ArrayList<Gare>();
        }
        gares.add(this);
    }

    public Gare(String name) {
        this.name = name;
        if (gares == null) {
            gares = new ArrayList<Gare>();
        }
        gares.add(this);
    }

    @Override
    public String toString() {
        return "Gare " + name;
    }

    public static String getGaresToString() {
        String liste = "";
        for (Gare gare : gares) {
            liste += "Gare " + gare.getName();
        }

        return liste;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
